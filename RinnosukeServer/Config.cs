﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Reflection;
using System.Xml;

namespace RinnosukeServer {
    static internal class Config {
        private static void CopyStream(Stream input, Stream output) {
            // Insert null checking here for production
            byte[] buffer = new byte[8192];

            int bytesRead;
            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0) {
                output.Write(buffer, 0, bytesRead);
            }
        }

        public static string? _configPath;

        public static string ConfigPath {
            get {
                _configPath ??= Server.Instance.IsMultiRoot ? ".\\Config\\rinnosuke.cfg" : "/etc/rinnosuke.cfg";
                return _configPath;
            }
            set => _configPath = value;
        }

        public class InvalidConfigurationException : Exception {
            public int? LineNumber;
            public string? KeyName;
            public InvalidConfigurationException() : base("The configuration file has invalid value(s).") {}
            public InvalidConfigurationException(int lineNumber) : base(string.Format("The configuration file has an invalid value at line {0}.", lineNumber)) { LineNumber = lineNumber; }
            public InvalidConfigurationException(string keyName) : base(string.Format("The configuration file has an invalid value in key {0}.", keyName)) { KeyName = keyName; }
            public InvalidConfigurationException(int lineNumber, string keyName) : base(string.Format("The configuration file has an invalid value in key {0} at line {1}.", keyName, lineNumber)) { LineNumber = lineNumber; KeyName = keyName; }
        }

        public static IPAddress PCBindAddress = new IPAddress(new byte[4]{0, 0, 0, 0});
        public static ushort PCPort = 1813;
        public static string SQLConnectionString = "Data Source=localhost;Initial Catalog=RinnosukeDB;Integrated Security=true;";

        public static void ReadConfig() {
            // Create a new file and write the example config to it if the config file does not exist
            if (!File.Exists(ConfigPath)){
                new FileInfo(ConfigPath).Directory.Create();
                using (Stream input = Assembly.GetExecutingAssembly().GetManifestResourceStream("RinnosukeServer.example-config.xml"))
                using (Stream output = File.Create(ConfigPath)) CopyStream(input, output);
            }

            var configReader = XmlReader.Create(ConfigPath);
            configReader.ReadToFollowing("RinnosukeConfiguration");
            configReader.ReadToFollowing("SQLConnectionString"); SQLConnectionString = configReader.ReadElementContentAsString();
            if (!Program.BindAddressSetInCommandLine) { configReader.ReadToFollowing("PCBindAddress"); if (!IPAddress.TryParse(configReader.ReadElementContentAsString(), out PCBindAddress)) throw new InvalidConfigurationException(((IXmlLineInfo)configReader).LineNumber, "PCBindAddress"); }
            if (!Program.PortSetInCommandLine) { configReader.ReadToFollowing("PCPort"); if (!ushort.TryParse(configReader.ReadElementContentAsString(), out PCPort)) throw new InvalidConfigurationException(((IXmlLineInfo)configReader).LineNumber, "PCPort"); }


        }
    }
}
