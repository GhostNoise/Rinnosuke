﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace RinnosukeShell {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App: Application {
        private static List<CultureInfo> m_Languages = new List<CultureInfo>();
        public static List<CultureInfo> Languages => m_Languages;
        /// <summary>An event to notify all application's windows</summary>
        public static event EventHandler? LanguageChanged;

        /// <summary>Current Language</summary>
        public static CultureInfo Language {
            get => System.Threading.Thread.CurrentThread.CurrentUICulture;
            set {
                if (value == null) throw new ArgumentNullException(nameof(value));
                if (value == System.Threading.Thread.CurrentThread.CurrentUICulture) return;

                // 1. Change application's language:
                System.Threading.Thread.CurrentThread.CurrentUICulture = value;

                //2. Create a ResourceDictionary for new culture
                ResourceDictionary newDictionary = new ResourceDictionary();
                switch (value.Name) {
                    case "ru-RU":
                        newDictionary.Source = new Uri(string.Format("Resources/Localization/lang.{0}.xaml", value.Name), UriKind.Relative);
                        break;
                    default:
                        newDictionary.Source = new Uri("Resources/Localization/lang.xaml", UriKind.Relative);
                        break;
                }

                //3. Find and delete old and add new ResourceDictionary
                ResourceDictionary? oldDictionary;
                try {
                    oldDictionary = (from dictionary in Current.Resources.MergedDictionaries
                                     where dictionary.Source != null && dictionary.Source.OriginalString.StartsWith("Resources/Localization/lang.")
                                     select dictionary).First();
                } catch (InvalidOperationException) { oldDictionary = null; }
                if (oldDictionary != null) {
                    int index = Current.Resources.MergedDictionaries.IndexOf(oldDictionary);
                    Current.Resources.MergedDictionaries.Remove(oldDictionary);
                    Current.Resources.MergedDictionaries.Insert(index, newDictionary);
                } else Current.Resources.MergedDictionaries.Add(newDictionary);

                //4. Call an Event to notify all windows.
                if (LanguageChanged != null) LanguageChanged(Current, new EventArgs());
            }
        }
        public App() {
            InitializeComponent();
            LanguageChanged += App_LanguageChanged;

            m_Languages.Clear();
            m_Languages.Add(new CultureInfo("en-US")); // Neutral (fallback) culture
            m_Languages.Add(new CultureInfo("ru-RU"));

            Language = new CultureInfo((RinnosukeShell.Properties.Settings.Default.DefaultLanguage == "default") ? CultureInfo.InstalledUICulture.Name : RinnosukeShell.Properties.Settings.Default.DefaultLanguage);
        }

        private void App_LanguageChanged(object? sender, EventArgs e){
            if (Language == CultureInfo.InstalledUICulture) return;
            RinnosukeShell.Properties.Settings.Default.DefaultLanguage = Language.Name;
            RinnosukeShell.Properties.Settings.Default.Save();
        }
    }
}
