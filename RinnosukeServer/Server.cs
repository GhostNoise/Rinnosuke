﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using RinnosukeProtocol;
using Microsoft.VisualBasic;

namespace RinnosukeServer {
    internal class Server {
        TcpListener? clientPCListener;

        TcpClient pendingClientPCConnection;

        SqlConnection sqlConnection;
        public enum Platform : byte {
            Windows, Linux, FreeBSD, Mac
        }
        public Platform CurrentPlatform;

        int ShutdownToken = new Random().Next(int.MinValue, int.MaxValue);

        /// <summary>
        /// Checks if the OS has multiple root points (e. g. "C:\", "D:\", "E:\" on Windows vs "/" on macOS, Linux and other Unix-like OSes)
        /// </summary>
        public bool IsMultiRoot => CurrentPlatform == Platform.Windows;

        public volatile bool
            IsInitialized = false,
            IsRunning = true,
            IsShutDown = false;

        /// <summary>
        /// A method wrapper for the IsInitialized property
        /// </summary>
        /// <returns>The value of IsInitialized</returns>
        public bool GetInitStatus() => IsInitialized;

        /// <summary>
        /// The platform the program is running on is unknown.
        /// </summary>
        public class UnknownPlatformException : Exception {
            public UnknownPlatformException() : base("You use an unknown platform. Please use Windows, macOS, Linux or FreeBSD.") { }
            public UnknownPlatformException(string message) : base(message) { }
        }

        private static Server? _instance;

        /// <summary>
        /// A singleton, why not?
        /// </summary>
        public static Server Instance {
            get { _instance ??= new Server(); return _instance; }
            set => _instance = value;
        }

        

        internal void Init() {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) CurrentPlatform = Platform.Windows;
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) CurrentPlatform = Platform.Linux;
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.FreeBSD)) CurrentPlatform = Platform.FreeBSD;
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) CurrentPlatform = Platform.Mac;
            else throw new UnknownPlatformException();
            Console.WriteLine(Program.START_MESSAGE);
            string PlatformMessage = "Running on ";
            switch (CurrentPlatform) {
                case Platform.Windows: PlatformMessage += "Windows"; break;
                case Platform.Linux: PlatformMessage += "Linux"; break;
                case Platform.FreeBSD: PlatformMessage += "FreeBSD"; break;
                case Platform.Mac: PlatformMessage += "Mac"; break;
            } Console.WriteLine(PlatformMessage);
            Config.ReadConfig();
            Console.Write("Trying to connect to the SQL database...");
            sqlConnection = new SqlConnection(Config.SQLConnectionString);
            sqlConnection.Open();
            Console.WriteLine("OK.");
            IPEndPoint clientPCEndPoint = new IPEndPoint(Config.PCBindAddress, Config.PCPort);
            Console.Write("Trying to bind {0}...", clientPCEndPoint);
            clientPCListener = new TcpListener(clientPCEndPoint); clientPCListener.Start();
            Console.WriteLine("OK.\nNow listening for client PC connections on {0}.", clientPCListener.LocalEndpoint);
            IsInitialized = true;
        }

        internal void Loop() {
            try { pendingClientPCConnection = clientPCListener.AcceptTcpClient(); } catch (SocketException) { return; }
            Console.WriteLine("A new client connected at {0}.", pendingClientPCConnection.Client.RemoteEndPoint);
            ThreadPool.QueueUserWorkItem(ProcessClient, pendingClientPCConnection);
        }

        internal void ProcessClient(object clientObj) {
            var client = (TcpClient)clientObj;
            var stream = client.GetStream(); int i;
            byte[] buffer = new byte[4096];
            string? data = null;
            RPDictionary.PacketType? packetType = null;
            var CheckIPSQLCommand = new SqlCommand("select PCID from PCs where PCIP = @IP", sqlConnection);
            CheckIPSQLCommand.Parameters.AddWithValue("IP", client.Client.RemoteEndPoint.ToString().Split(':')[0]);
            var CheckIPSQLCommandReader = CheckIPSQLCommand.ExecuteReader(); var PCExistsInDB = false; int? PCID;
            while (CheckIPSQLCommandReader.Read()) {
                PCID = (int?)CheckIPSQLCommandReader.GetValue("PCID");
                if (!PCExistsInDB) PCExistsInDB = PCID != null;
            } if (!PCExistsInDB) {
                Console.WriteLine("The computer is not registered in the database.");
                var msg = string.Format("M{0}\n{1}",
                        (byte)RPDictionary.ServerToClientMessageType.Error,
                        (byte)RPDictionary.Errors.UnregisteredPC);
                stream.Write(Encoding.UTF8.GetBytes(msg));
                CheckIPSQLCommandReader.Close(); return;
            }
            stream.Write(Encoding.UTF8.GetBytes(string.Format("M{0}\n", RPDictionary.ServerToClientMessageType.GetStatus)));
            while (client.Connected) {
                 while ((i = stream.Read(buffer, 0, buffer.Length)) != 0) {
                    data = Encoding.UTF8.GetString(buffer, 0, i);
                    if (data == ShutdownToken.ToString()) return;
                    packetType = RPDictionary.GetPacketType(data);
                    switch (packetType) {
                        case RPDictionary.PacketType.Message: 
                            var MessageArray = data.Split('\n');
                            RPDictionary.ClientMessageType messageType = (RPDictionary.ClientMessageType)byte.Parse(MessageArray[0][1..]);
                            if (messageType != RPDictionary.ClientMessageType.KeepAlive) {
                                var args = MessageArray[1..];
                            } else stream.Write(Encoding.UTF8.GetBytes(string.Format("R{0}", RPDictionary.ServerToClientResponseType.MessageReceived)));
                        break;
                        case RPDictionary.PacketType.Response: {

                        } break;
                        case RPDictionary.PacketType.Unknown: default:
                            stream.Write(Encoding.UTF8.GetBytes(string.Format("R{0}\n{1}", RPDictionary.ServerToClientResponseType.Error, RPDictionary.Errors.MalformedPackage))); break;
                    }
                }
            }
        }

        internal void ShutDown() {
            Console.WriteLine("Stopping the server...\nDisconnecting from SQL database...");
            sqlConnection.Close();
            Console.WriteLine("Stopping the TCP listener on {0}...", clientPCListener.LocalEndpoint);
            clientPCListener.Stop(); clientPCListener = null;

            IsRunning = false;
            Console.WriteLine("Goodbye!");
            IsShutDown = true;
        }

        internal void HandleSigInt() {
            Console.WriteLine("SIGINT received, shutting down server...");
            if (!IsShutDown) ShutDown();
            else Console.WriteLine("Server already shut down.");
            Process.GetCurrentProcess().CloseMainWindow();
        }

        internal void HandleSigTerm() {
            Console.WriteLine("SIGTERM received, shutting down server...");
            if (!IsShutDown) ShutDown();
            else Console.WriteLine("Server already shut down.");
        }
    }
}
