﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace RinnosukeProtocol {
    public static class RPDictionary {
        public enum PacketType: byte {
            Unknown, Message, Response
        }
        public enum SenderType: byte {
            Error, ServerToClient, ServerToAdmin, Client, Admin
        }
        public enum Client_Message_Type: byte {
            /// <summary>This message is being sent once in KeepAliveInterval ms to determine the connection status.</summary>
            KeepAlive,
            /// <summary>This message is being sent as a request to get a customer session ID from provided auth data.</summary>
            AuthRequest,
            /// <summary>This message is being sent when a client wants to see their balance.</summary>
            GetBalanceRequest,
            /// <summary>This message is being sent when a client requests a session start.</summary>
            SessionStartRequest,
            /// <summary>This message is being sent if the client requests a session extension.</summary>
            SessionExtendRequest,
            /// <summary>This message is being sent if the client wants to end their session.</summary>
            SessionEndRequest,
            /// <summary>This message is being used when an admin wants to set up a client PC.</summary>
            AdminAccessRequest
        }
        public enum Client_Response_Type: byte {
            /// <summary>Message received successfully, used when a detailed response is not needed (e. g. to a KeepAlive message).</summary>
            MessageReceived,
            /// <summary>Used when an error has occured (e. g. a malformed message).</summary>
            Error,
            /// <summary>Respond with current status.</summary>
            StatusResponse
        }
        public enum Admin_Message_Type : byte { }
        public enum Admin_Response_Type : byte { }
        /// <summary>An enum containing codes of messages the server sends to a client PC.</summary>
        public enum ServerToClient_Message_Type: byte {
            /// <summary>This message is being sent once in KeepAliveInterval ms to determine the connection status.</summary>
            KeepAlive,
            /// <summary>This message is being sent first if the PC is unknown</summary>
            Error,
            /// <summary>This message is being sent when the server should begin the session on the client PC.</summary>
            BeginSession,
            /// <summary>This message is being sent when the server should end the session on the client PC.</summary>
            EndSession,
            /// <summary>This message is being sent when the server should extend the client's session.</summary>
            ExtendSession,
            /// <summary>This message is being sent when the server wants to get the client's status.</summary>
            GetStatus,
            /// <summary>This message is being sent when the server wants the client to execute a command.</summary>
            RemoteCMD
        }
        public enum ServerToClient_Response_Type: byte {
            /// <summary>Message received successfully, used when a detailed response is not needed (e. g. to a KeepAlive message).</summary>
            MessageReceived,
            /// <summary>Used when an error has occured (e. g. a malformed message).</summary>
            Error,
            /// <summary>Used when the client requests a new session.</summary>
            AuthToken,
            /// <summary>Used when the server successfully fulfills a request.</summary>
            RequestFulfilled,
            /// <summary>Used when the server denies a request (e. g. the client requested a session extension but didn't have enough money).</summary>
            RequestDenied
        }
        public enum ServerToAdmin_Message_Type : byte { KeepAlive }
        public enum ServerToAdmin_Response_Type : byte { MessageReceived, Error, StatusResponse, AuthToken, AuthError }
        public enum ClientRequestDenialReason : byte {
            /// <summary>Fallback reason, shouldn't be used normally.</summary>
            Unknown,
            /// <summary> Used when the client doesn't have enough money.</summary>
            NotEnoughFunds,
            /// <summary>Used when the client supplied wrong data in an authentification request.</summary>
            WrongAuthData
        }

        public enum Errors: byte {
            MalformedPackage,
            UnregisteredPC
        }
        public static PacketType GetPacketType(string packet) {
            if (string.IsNullOrEmpty(packet)) return PacketType.Unknown;
            else switch (packet[0]) {
                case 'M': return PacketType.Message;
                case 'R': return PacketType.Response;
                default: return PacketType.Unknown;
            }
        }
        public abstract class Packet {
            public abstract PacketType PType { get; }
            public abstract SenderType SenderType { get; }
            public abstract string[] Args { get; }
            public abstract byte Code { get; }
        }

        public class UnknownPacket : Packet {
            public override PacketType PType => PacketType.Unknown;
            public override SenderType SenderType => SenderType.Error;

            public override string[] Args => new string[0];
            public override byte Code => 255;
        }

        public abstract class Message : Packet {
            public override PacketType PType => PacketType.Message;
            public Guid MessageID;
            public List<Response> Responses;

            public override string ToString() => string.Format("M{0}\n{1}\n{2}", Code, MessageID, string.Join("\n", Args));
        }
        public abstract class Response : Packet {
            public override PacketType PType => PacketType.Response;
            public Guid RespondedMessageID;
            public Message RespondedMessage;

            public override string ToString() => string.Format("R{0}\n{1}\n{2}", Code, RespondedMessageID, string.Join("\n", Args));
        }

        public abstract class ServerToAdmin_Message : Message {
            public override SenderType SenderType => SenderType.ServerToAdmin;
            public ServerToAdmin_Message_Type MessageType => (ServerToAdmin_Message_Type)Code;
        }

        public abstract class ServerToAdmin_Response : Response {
            public override SenderType SenderType => SenderType.ServerToAdmin;
            public ServerToAdmin_Response_Type ResponseType => (ServerToAdmin_Response_Type)Code;
        }

        public abstract class ServerToClient_Message : Message {
            public override SenderType SenderType => SenderType.ServerToClient;
            public ServerToClient_Message_Type MessageType => (ServerToClient_Message_Type)Code;
        }

        public abstract class ServerToClient_Response : Response {
            public override SenderType SenderType => SenderType.ServerToClient;
            public ServerToClient_Response_Type ResponseType => (ServerToClient_Response_Type)Code;
        }

        public abstract class Admin_Message : Message {
            public override SenderType SenderType => SenderType.Admin;
            public Admin_Message_Type MessageType => (Admin_Message_Type)Code;
        }

        public abstract class Admin_Response : Response {
            public override SenderType SenderType => SenderType.Admin;
            public Admin_Response_Type ResponseType => (Admin_Response_Type)Code;
        }

        public abstract class Client_Message : Message {
            public override SenderType SenderType => SenderType.Client;
            public Client_Message_Type MessageType => (Client_Message_Type)Code;
        }

        public abstract class Client_Response : Response {
            public override SenderType SenderType => SenderType.Client;
            public Client_Response_Type ResponseType => (Client_Response_Type)Code;
        }

        public class KeepAlive_ServerToClient_Message: ServerToClient_Message {
            public override byte Code => (byte)ServerToClient_Message_Type.KeepAlive;
            public override string[] Args => new string[0];
        }

        public class KeepAlive_Client_Message: Client_Message {
            public override byte Code => (byte)Client_Message_Type.KeepAlive;
            public override string[] Args => new string[0];
        }
    }
}
