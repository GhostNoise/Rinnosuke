﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RinnosukeShell {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow: Window {
        bool MouseInMenuButton = false, 
            MouseInShutdownButton = false;
        private DispatcherTimer timer;

        private static string UserDesktopPath => Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        private static string CommonDesktopPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory);
        private List<FileIcon> AllFilesOnDesktop = new List<FileIcon>();

        

        public MainWindow() {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 250);
            timer.Start();
            RefreshDesktop();
            var watcher = new FileSystemWatcher(UserDesktopPath);
            watcher.Changed += (object sender, FileSystemEventArgs e) => RefreshDesktop();
            LBDesktop.ItemsSource = AllFilesOnDesktop;
        }

        private void RefreshDesktop() {
            AllFilesOnDesktop.Clear();
            foreach (string path in Directory.GetFiles(UserDesktopPath, "*.*", SearchOption.TopDirectoryOnly))
                CheckAndAddToAllFiles(new FileIcon(path));
            foreach (string path in Directory.GetDirectories(UserDesktopPath, "*.*", SearchOption.TopDirectoryOnly))
                CheckAndAddToAllFiles(new FileIcon(path));
            foreach (string path in Directory.GetFiles(CommonDesktopPath, "*.*", SearchOption.TopDirectoryOnly))
                CheckAndAddToAllFiles(new FileIcon(path));
            foreach (string path in Directory.GetDirectories(CommonDesktopPath, "*.*", SearchOption.TopDirectoryOnly))
                CheckAndAddToAllFiles(new FileIcon(path));
        }

        private void CheckAndAddToAllFiles(FileIcon file) {
            if (!file.IsHidden) AllFilesOnDesktop.Add(file);
            else if (FileIcon.ShowHiddenFiles)
                if (!file.IsSystem) AllFilesOnDesktop.Add(file);
                else if (FileIcon.ShowHiddenSystemFiles) AllFilesOnDesktop.Add(file);

        }

        // Updating the desktop wallpaper every 250ms can be a bad idea, but attaching a registry listener would probably be even worse, so I picked the lesser evil.
        // And by the way, it's absolutely not CPU-heavy (2% of an AMD FX), so it's probably not that bad.
        private void timer_Tick(object? sender, EventArgs e) {
            TimeBlock.Text = DateTime.Now.ToString("H:mm:ss");
#pragma warning disable CS8602 // Those registry keys can't be null on a proper Windows installation, so these warnings will be suppressed for this code part.
            var WallpaperPath = Registry.CurrentUser.OpenSubKey("Control Panel").OpenSubKey("Desktop").GetValue("Wallpaper").ToString();
            var DesktopRGB = Array.ConvertAll(Registry.CurrentUser.OpenSubKey("Control Panel").OpenSubKey("Colors").GetValue("Background").ToString().Split(" "), b => byte.Parse(b));
#pragma warning restore CS8602
            Background = string.IsNullOrWhiteSpace(WallpaperPath)
                ? new SolidColorBrush(Color.FromRgb(DesktopRGB[0], DesktopRGB[1], DesktopRGB[2]))
                : new ImageBrush(new BitmapImage(new Uri(WallpaperPath)));
        }


        private void MenuButton_MouseEnter(object sender, MouseEventArgs e) {
            if (!MouseInMenuButton) {
                foreach (GeometryDrawing drawing in MenuButtonDrawingGroup.Children) drawing.Brush = new SolidColorBrush(Color.FromArgb(255, 127, 127, 127));
                MouseInMenuButton = true;
            }
        }

        private void MenuButton_MouseLeave(object sender, MouseEventArgs e) {
            if (MouseInMenuButton) {
                foreach (GeometryDrawing drawing in MenuButtonDrawingGroup.Children) drawing.Brush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                MouseInMenuButton = false;
            }
        }

        private void MenuButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            foreach (GeometryDrawing drawing in MenuButtonDrawingGroup.Children) drawing.Brush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
        }

        private void MenuButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            SolidColorBrush NewBrush;
            if (MouseInMenuButton) NewBrush = new SolidColorBrush(Color.FromArgb(255, 127, 127, 127));
            else NewBrush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            foreach (GeometryDrawing drawing in MenuButtonDrawingGroup.Children) drawing.Brush = NewBrush;
        }

        private void ShutdownButton_MouseEnter(object sender, MouseEventArgs e) {
            if (!MouseInShutdownButton) {
                foreach (GeometryDrawing drawing in ShutdownButtonDrawingGroup.Children) drawing.Brush = new SolidColorBrush(Color.FromArgb(255, 127, 127, 127));
                MouseInShutdownButton = true;
            }
        }

        private void ShutdownButton_MouseLeave(object sender, MouseEventArgs e) {
            if (MouseInShutdownButton) {
                foreach (GeometryDrawing drawing in ShutdownButtonDrawingGroup.Children) drawing.Brush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                MouseInShutdownButton = false;
            }
        }

        private void ShutdownButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            foreach (GeometryDrawing drawing in ShutdownButtonDrawingGroup.Children) drawing.Brush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
        }

        private void ShutdownButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            SolidColorBrush NewBrush;
            if (MouseInShutdownButton) NewBrush = new SolidColorBrush(Color.FromArgb(255, 127, 127, 127));
            else NewBrush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            foreach (GeometryDrawing drawing in ShutdownButtonDrawingGroup.Children) drawing.Brush = NewBrush;
        }
        
        private void MenuButton_Click(object sender, RoutedEventArgs e) => MainMenu.IsOpen = true;

        private void StartNewOrExtendCurrentSession(object sender, RoutedEventArgs e) => new Resources.Popups.PagePopup(new Resources.Pages.CustomerAuthPage()).ShowDialog();

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e) => Process.Start("explorer", "\"" + ((FileIcon)((ListViewItem)sender).DataContext).FullPath + "\"");

        private void ShutdownButton_Click(object sender, RoutedEventArgs e) => new Resources.Popups.ShutdownPopup().ShowDialog();
    }
}
