/* RinnosukeDB creation script */
create table Rooms (
	RoomID int not null primary key,
	RoomName varchar(128) unique,
	RoomPrice money not null
)
create table PCs (
	PCID int not null primary key,
	PCRoom int not null foreign key references Rooms(RoomID),
	PCIP varchar(128) not null, -- IP stored as string to ensure maximum compatibility (IPv4/IPv6)
	PCAlias varchar(256)
)
go
create view Random as select rand() 'RandomValue'
go
create function RandomString(@Length tinyint) returns varchar(255) as begin
	declare @CharPool varchar(255), @PoolLength int, @LoopCount tinyint, @RandomString varchar(255)
	set @CharPool = 
	    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789.,-_!$@#%^&*'
	set @PoolLength = len(@CharPool)
	
	set @LoopCount = 0
	set @RandomString = ''
	
	while (@LoopCount < @Length) begin
	    select @RandomString = @RandomString + 
	        substring(@Charpool, convert(int, (select RandomValue from Random) * @PoolLength) + 1, 1)
	    select @LoopCount = @LoopCount + 1
	end
	return @RandomString
end
go
create table Customers (
	CustomerID int not null primary key,
	CustomerName varchar(255) not null,
	CustomerPhone int not null unique check (CustomerPhone between 79000000000 and 79999999999), -- Russian cellphone number format, change if necessary
	CustomerBalance money not null default 0,
	CustomerPasswordHash varbinary(32) not null,
	CustomerPasswordSalt char(16) not null
)
go
create procedure RegisterCustomer
@CustomerID int, @CustomerName varchar(255), @CustomerPhone int, @CustomerPassword varchar(16) as begin
	declare @salt char(16) = dbo.RandomString(16)
	insert into Customers (CustomerID, CustomerName, CustomerPhone, CustomerPasswordSalt, CustomerPasswordHash) values
	(@CustomerID, @CustomerName, @CustomerPhone, @salt, hashbytes('SHA2_256', concat(@CustomerPassword, @salt)))
end
go
create function CheckCustomerAuth (@CustomerID int, @CustomerPassword varchar(16)) returns bit as begin
	declare @Result bit
	if exists (select CustomerID from Customers where CustomerID = @CustomerID and CustomerPasswordHash = hashbytes('SHA2_256', concat(@CustomerPassword, CustomerPasswordSalt))) set @Result = 0
	else set @Result = 1
	return @Result
end
go
create procedure ChangeCustomerPassword
@CustomerID int, @CustomerPassword varchar(16) as begin
	declare @salt char(16) = dbo.RandomString(16)
	update Customers
	set CustomerPasswordHash = hashbytes('SHA2_256', concat(@CustomerPassword, @salt)), CustomerPasswordSalt = @salt
	where CustomerID = @CustomerID
end
go
create table StaffMembers (
	StaffMemberID int not null primary key,
	StaffMemberName varchar(255) not null,
	StaffMemberPhone int not null unique check (StaffMemberPhone between 79000000000 and 79999999999), -- Russian cellphone number format, change if necessary
	StaffMemberPasswordHash varbinary(32) not null,
	StaffMemberPasswordSalt char(16) not null
)
go
create procedure RegisterStaffMember
@StaffMemberID int, @StaffMemberName varchar(255), @StaffMemberPhone int, @StaffMemberPassword varchar(16) as begin
	declare @salt char(16) = dbo.RandomString(16)
	insert into StaffMembers (StaffMemberID, StaffMemberName, StaffMemberPhone, StaffMemberPasswordSalt, StaffMemberPasswordHash) values
	(@StaffMemberID, @StaffMemberName, @StaffMemberPhone, @salt, hashbytes('SHA2_256', concat(@StaffMemberPassword, @salt)))
end
go
create function CheckStaffMemberAuth (@StaffMemberID int, @StaffMemberPassword varchar(16)) returns bit as begin
	declare @Result bit
	if exists (select StaffMemberID from StaffMembers where StaffMemberID = @StaffMemberID and StaffMemberPasswordHash = hashbytes('SHA2_256', concat(@StaffMemberPassword, StaffMemberPasswordSalt))) set @Result = 0
	else set @Result = 1
	return @Result
end
go
create procedure ChangeStaffMemberPassword
@StaffMemberID int, @StaffMemberPassword varchar(16) as begin
	declare @salt char(16) = dbo.RandomString(16)
	update StaffMembers
	set StaffMemberPasswordHash = hashbytes('SHA2_256', concat(@StaffMemberPassword, @salt)), StaffMemberPasswordSalt = @salt
	where StaffMemberID = @StaffMemberID
end
go
create function GetRoomPriceByPCID (@TargetPCID int) returns money as begin
	return (select RoomPrice from Rooms where (select PCRoom from PCs where PCID=@TargetPCID) = RoomID)
end
go
create function ComputeCafeSessionPrice (@SessionStart datetime, @SessionEnd datetime, @SessionPC int) returns money as begin
	return (select (dbo.GetRoomPriceByPCID(@SessionPC) * (datediff(hour, @SessionStart, @SessionEnd) + (datediff(minute, @SessionStart, @SessionEnd)/60) + (datediff(second, @SessionStart, @SessionEnd)/3600))))
end
go
create table CafeSessions (
	SessionID int not null primary key,
	SessionStart datetime not null,
	SessionEnd datetime not null,
	SessionPC int not null foreign key references PCs(PCID),
	SessionCustomer int not null foreign key references Customers(CustomerID),
	SessionRegistrator int not null foreign key references StaffMembers(StaffMemberID),
	SessionPrice money not null
)
go
create trigger CafeSessions_OnInsert on CafeSessions instead of insert as
insert into CafeSessions (SessionID, SessionStart, SessionEnd, SessionPC, SessionCustomer, SessionPrice)
select SessionID, SessionStart, SessionEnd, SessionPC, SessionCustomer, isnull(SessionPrice, dbo.ComputeCafeSessionPrice(SessionStart, SessionEnd, SessionPC)) -- This is the solution for making a computed default value.
from inserted
