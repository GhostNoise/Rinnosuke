﻿using System;
using System.Net;
using System.Runtime.InteropServices;

namespace RinnosukeServer {
    public static class Program {
        public const string START_MESSAGE = "Rinnosuke Server v0.0.0d";

        private enum NextArg : byte {
            Default, Port, ConfigPath, BindAddress
        }

        private static NextArg _nextArg = NextArg.Default;

        public class InvalidArgumentException : Exception {
            public InvalidArgumentException() : base("One or more argument(s) is(are) invalid.\".\nUse \"rinnosuke-server -h\" for help.") { }
            public InvalidArgumentException(string message) : base(message) { }
            void Throw() => throw new InvalidArgumentException();
            void Throw(string message) => throw new InvalidArgumentException(message);
        }

        public class HelpPseudoException : Exception {
            public HelpPseudoException() : base("Usage: rinnosuke-server [-b/--bind-address IP] [-c/--config-file path/to/config.file] [-p/--port port number] [-h/--help]") { }
        }

        public static bool PortSetInCommandLine = false,
            BindAddressSetInCommandLine = false,
            ConfigFilePathSetInCommandLine = false;

        public static void Main(string[] args) {
            try { ParseArgs(args); } catch (InvalidArgumentException e) {
                Console.WriteLine("An InvalidArgumentException has occured:\n" + e.Message);
                Environment.Exit(-1);
            } catch (HelpPseudoException e) {
                Console.WriteLine(e.Message);
                Environment.Exit(0);
            }

            Server.Instance.Init();
            Console.CancelKeyPress += delegate { Server.Instance.HandleSigInt(); };
            AppDomain.CurrentDomain.ProcessExit += delegate { Server.Instance.HandleSigTerm(); };
            WaitUntilServerFinishesInitializing();
            while (Server.Instance.IsRunning) Server.Instance.Loop();
            
        }

        private static async void WaitUntilServerFinishesInitializing() {
            await TaskEx.WaitUntil(Server.Instance.GetInitStatus);
        }

        private static void ParseArgs(string[] args) {
            foreach (string arg in args) {
                switch (_nextArg) {
                    case NextArg.Default:
                        if (arg[0] == '-') if (arg.Length != 1 && arg[1] != '-') if (_nextArg == NextArg.Default) foreach (char c in arg) switch (c) {
                                            case 'b': _nextArg = NextArg.BindAddress; break;
                                            case 'c': _nextArg = NextArg.ConfigPath; break;
                                            case '?':
                                            case 'h': throw new HelpPseudoException();
                                            case 'p': _nextArg = NextArg.Port; break;
                                            case '-': break;
                                            default: throw new InvalidArgumentException("Unknown key \"-" + c + "\".\nUse \"rinnosuke-server -h\" for help.");
                                        }
                                else throw new InvalidArgumentException("You have to specify additional data after one of the keys.");
                            else if (arg[1] == '-') switch (arg){
                                    case "--bind-address": _nextArg = NextArg.BindAddress; break;
                                    case "--config-file": _nextArg = NextArg.ConfigPath; break;
                                    case "--help": throw new HelpPseudoException();
                                    case "--port": _nextArg = NextArg.Port; break;
                                    default: throw new InvalidArgumentException("Unknown key " + arg + ".\nUse \"rinnosuke-server -h\" for help.");
                                } else throw new InvalidArgumentException("Too many arguments.");
                        break;
                    case NextArg.Port: if (!ushort.TryParse(arg, out Config.PCPort)) throw new InvalidArgumentException("Invalid port number format.\nYou have to specify a valid integer in range between 0 and 65535.");
                    if (PortSetInCommandLine) throw new InvalidArgumentException("You cannot have multiple -p keys.");
                        PortSetInCommandLine = true; _nextArg = NextArg.Default; break;
                    case NextArg.BindAddress:
                        if (!IPAddress.TryParse(arg, out Config.PCBindAddress)) throw new InvalidArgumentException("Invalid IP address format.\nYou have to specify a valid IP address.");
                        if (BindAddressSetInCommandLine) throw new InvalidArgumentException("You cannot have multiple -b keys.");
                        BindAddressSetInCommandLine = true; _nextArg = NextArg.Default; break;
                    case NextArg.ConfigPath: if (!File.Exists(arg)) throw new InvalidArgumentException("The specified file does not exist, you don't have permission to read it or the provided string is not a valid path.");
                        if (ConfigFilePathSetInCommandLine) throw new InvalidArgumentException("You cannot have multiple -c keys.");
                        ConfigFilePathSetInCommandLine = true; _nextArg = NextArg.Default; Config.ConfigPath = arg; break;
                }
            }
            if (_nextArg != NextArg.Default) throw new InvalidArgumentException("You have to specify additional data after one of the keys.");
        }
    }
}